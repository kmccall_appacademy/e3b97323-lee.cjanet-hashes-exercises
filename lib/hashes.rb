# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length = {}
  str.split.each {|word| word_length[word]=word.length}
  word_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k,v| v}[-1][0]
end

# p greatest_key_by_val({ 10 => 1, 2 => 5 })

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result.
# march = {rubies: 10,
# emeralds: 14, diamonds: 2}
# april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}


def update_inventory(older, newer)
  newer.each { |rock, quantity| older[rock] = quantity}
  older
end


# p update_inventory(march, april) == {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}





# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new(0)
  word.each_char {|chr| letter_count[chr]+=1}
  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  keys_only = Hash.new(0)
  arr.each {|el| keys_only[el]+=1}
  keys_only.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_count = Hash.new(0)
  numbers.each {|num| num.odd? ? even_odd_count[:odd]+=1 : even_odd_count[:even]+=1 }
  even_odd_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  most_common = 'a'
  string.each_char {|chr| most_common = chr if string.count(chr) > string.count(most_common)}
  most_common
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12).

# students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]


def fall_and_winter_birthdays(students)
  students.select {|student, month| month > 6}.keys.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9


def biodiversity_index(specimens)
  specimens_count = Hash.new(0)
  specimens.each {|specimen| specimens_count[specimen]+=1}

  number_of_species = specimens_count.keys.count
  smallest_population_size = specimens_count.values.min
  largest_population_size = specimens_count.values.max

  number_of_species**2 * smallest_population_size / largest_population_size
end

# p biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"])

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true


def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_count = character_count(normal_sign)
  character_count(vandalized_sign).all? do |chr, count|
    normal_sign_count[chr] >= count
  end
end

def character_count(str)
  str.delete!(" ")
  letter_count = Hash.new(0)
  str.each_char {|chr| letter_count[chr.downcase]+=1 if ('a'..'z').include?(chr.downcase)}
  letter_count
end
